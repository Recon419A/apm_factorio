-- Requires Defines------------------------------------------------------------
--
--
-- ----------------------------------------------------------------------------
local updates = require('lib.updates')
local solar = require('__apm_lib__.lib.script.solar')

-- Function -------------------------------------------------------------------
--
--
-- ----------------------------------------------------------------------------
local function On_Init()
    updates.run()
    solar.init()
    remote.call('apm_solar', 'add_surface', 1)
end

-- Function -------------------------------------------------------------------
--
--
-- ----------------------------------------------------------------------------
local function on_load()
end

-- Function -------------------------------------------------------------------
--
--
-- ----------------------------------------------------------------------------
local function on_update()
    updates.run()
    solar.init()
end

-- Function -------------------------------------------------------------------
--
--
-- ----------------------------------------------------------------------------
local function event_on_tick(event)
    solar.on_tick()
end

-- Function -------------------------------------------------------------------
--
--
-- ----------------------------------------------------------------------------
local function event_mod_setting_changed(event)
    solar.mod_setting_changed()
end

-- Event Defines---------------------------------------------------------------
--
--
-- ----------------------------------------------------------------------------
script.on_event(defines.events.on_tick, function(event) event_on_tick(event) end)
script.on_event (defines.events.on_runtime_mod_setting_changed, function(event) event_mod_setting_changed(event) end)

-- ----------------------------------------------------------------------------
-- ----------------------------------------------------------------------------
script.on_init(On_Init)
script.on_load(on_load)
script.on_configuration_changed(on_update)