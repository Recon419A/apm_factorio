if not seafloor_pumps then seafloor_pumps = {} end
if not seafloor_pumps.setup then seafloor_pumps.setup = {} end

-- Definitions ----------------------------------------------------------------
--
--
-- ----------------------------------------------------------------------------
local apm_angel_addon_electric_seafloor_pump = settings.startup['apm_angel_addon_electric_seafloor_pump'].value

-- Function -------------------------------------------------------------------
--
--
-- ----------------------------------------------------------------------------
local function setup_electric(entity)
    local direction = entity.direction
    local position = entity.position
    local surface = entity.surface
    local force = entity.force

    surface.create_entity{name="apm_seafloor_pump_electric", position=position, direction=direction, force=force,
                          fast_replace=true, spill=false, raise_built=true, create_build_effect_smoke=false,
                          raise_built=true}
    entity.destroy({raised_destroy=true})
end

-- Function -------------------------------------------------------------------
--
--
-- ----------------------------------------------------------------------------
local function rescan()
    if not apm_angel_addon_electric_seafloor_pump then return end

    log('-- rescan for seafloor pumps --------------------------------------------------')

    local count_electric = 0

    for _, surface in pairs(game.surfaces) do
        local offshore_pumps = surface.find_entities_filtered{type='offshore-pump'}
        for _, entity in pairs(offshore_pumps) do
            if entity.name == 'seafloor-pump' then
                setup_electric(entity)
                count_electric = count_electric +1
            end
        end
    end
    log('converted to electric seafloor pump: ' ..tostring(count_electric))
    log('-- ----------------------------------------------------------------------------')
end

-- Function -------------------------------------------------------------------
--
--
-- ----------------------------------------------------------------------------
function seafloor_pumps.on_build(entity)
    if not apm_angel_addon_electric_seafloor_pump then return end

    if not entity then return end
    if not entity.valid then return end
    if not entity.type == 'offshore-pump' then return end

    if entity.name == 'seafloor-pump' then
        setup_electric(entity)
    end
end

-- Function -------------------------------------------------------------------
--
--
-- ----------------------------------------------------------------------------
function seafloor_pumps.on_init()
    rescan()
end

-- Function -------------------------------------------------------------------
--
--
-- ----------------------------------------------------------------------------
function seafloor_pumps.on_update()
    apm_angel_addon_electric_seafloor_pump = settings.startup['apm_angel_addon_electric_seafloor_pump'].value
    rescan()
end

-- Remote Interface ------------------------------------------------------------
--
--
-- ----------------------------------------------------------------------------
-- /c remote.call('apm_seafloor_pumps', 'rescan')
remote.add_interface("apm_seafloor_pumps",{
    rescan = function() return rescan() end
    })

-- ----------------------------------------------------------------------------
-- ----------------------------------------------------------------------------
return seafloor_pumps