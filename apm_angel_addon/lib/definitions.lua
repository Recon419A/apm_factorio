require('__apm_lib__.lib.log')

local self = 'apm_angel_addon/lib/definitions.lua'

APM_LOG_HEADER(self)

if apm.angel_addon.casting_machine == nil then apm.angel_addon.casting_machine = {} end
if apm.angel_addon.recipes == nil then apm.angel_addon.recipes = {} end
if apm.angel_addon.constants == nil then apm.angel_addon.constants = {} end
if apm.angel_addon.icons == nil then apm.angel_addon.icons = {} end
if apm.angel_addon.icons.path == nil then apm.angel_addon.icons.path = {} end

-- Constants ------------------------------------------------------------------
--
--
-- ----------------------------------------------------------------------------
apm.angel_addon.constants.emissions = {}
apm.angel_addon.constants.emissions.seafloor_pump = 4

apm.angel_addon.constants.energy_usage = {}
apm.angel_addon.constants.energy_usage.seafloor_pump = '540kW'

apm.angel_addon.constants.modules = {}
apm.angel_addon.constants.modules.specification_seafloor_pump = {module_slots = 3}
apm.angel_addon.constants.modules.allowed_effects_seafloor_pump = {"consumption", "speed", "productivity", "pollution"}

-- Icon path ------------------------------------------------------------------
--
--
-- ----------------------------------------------------------------------------
 apm.angel_addon.icons.path.gear_casting_form = '__apm_resource_pack__/graphics/icons/apm_mold_gears.png'
 apm.angel_addon.icons.path.ball_casting_form = '__apm_resource_pack__/graphics/icons/apm_mold_balls.png'

-- Icons ----------------------------------------------------------------------
--
--
-- ----------------------------------------------------------------------------
apm.angel_addon.icons.gear_casting_form = {icon=apm.angel_addon.icons.path.gear_casting_form, icon_size=64}
apm.angel_addon.icons.ball_casting_form = {icon=apm.angel_addon.icons.path.ball_casting_form, icon_size=64}
