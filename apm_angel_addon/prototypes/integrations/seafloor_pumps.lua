require ('util')
require('__apm_lib__.lib.log')

local self = 'apm_angel_addon/prototypes/integrations/seafloor_pumps.lua'

APM_LOG_HEADER(self)

local apm_angel_addon_casting_machine_recipes = settings.startup["apm_angel_addon_casting_machine_recipes"].value
local apm_angel_addon_strand_casting_machine_recipes = settings.startup["apm_angel_addon_strand_casting_machine_recipes"].value
local apm_angel_addon_casting_recipes = settings.startup["apm_angel_addon_casting_recipes"].value
local apm_angel_addon_always_show_made_in = settings.startup["apm_angel_addon_always_show_made_in"].value
local apm_angel_addon_steam_unification = settings.startup["apm_angel_addon_steam_unification"].value
local apm_angel_addon_compat_bob = settings.startup["apm_angel_addon_compat_bob"].value
local apm_angel_addon_compat_shiny_angel_gfx = settings.startup["apm_angel_addon_compat_shiny_angel_gfx"].value
local apm_angel_addon_electric_seafloor_pump = settings.startup["apm_angel_addon_electric_seafloor_pump"].value

APM_LOG_SETTINGS(self, 'apm_angel_addon_casting_machine_recipes', apm_angel_addon_casting_machine_recipes)
APM_LOG_SETTINGS(self, 'apm_angel_addon_strand_casting_machine_recipes', apm_angel_addon_strand_casting_machine_recipes)
APM_LOG_SETTINGS(self, 'apm_angel_addon_casting_recipes', apm_angel_addon_casting_recipes)
APM_LOG_SETTINGS(self, 'apm_angel_addon_always_show_made_in', apm_angel_addon_always_show_made_in)
APM_LOG_SETTINGS(self, 'apm_angel_addon_steam_unification', apm_angel_addon_steam_unification)
APM_LOG_SETTINGS(self, 'apm_angel_addon_compat_bob', apm_angel_addon_compat_bob)
APM_LOG_SETTINGS(self, 'apm_angel_addon_compat_shiny_angel_gfx', apm_angel_addon_compat_shiny_angel_gfx)
APM_LOG_SETTINGS(self, 'apm_angel_addon_electric_seafloor_pump', apm_angel_addon_electric_seafloor_pump)

if not apm_angel_addon_electric_seafloor_pump then return end

-- Recipe ---------------------------------------------------------------------
--
--
-- ----------------------------------------------------------------------------
local recipe = {}
recipe.type = "recipe"
recipe.name = "apm_water_viscous_mud_from_void"
recipe.category = 'apm_fluids_from_the_void'
recipe.enabled = true
recipe.hidden = true
recipe.energy_required = 1
recipe.ingredients = {}
recipe.results = {{type='fluid', name='water-viscous-mud', amount=300}}
recipe.main_product = 'water-viscous-mud'
data:extend({recipe})

-- Electric seefloor pump -----------------------------------------------------
--
--
-- ----------------------------------------------------------------------------
local seafloor_pump = data.raw['offshore-pump']['seafloor-pump']
seafloor_pump.fast_replaceable_group = 'apm_seafloor_pump'

local seafloor_pump_electric = {}
seafloor_pump_electric.type = "assembling-machine"
seafloor_pump_electric.name = "apm_seafloor_pump_electric"
seafloor_pump_electric.localised_name = {"entity-name.seafloor-pump"}
seafloor_pump_electric.localised_description = {"entity-description.seafloor-pump"}
seafloor_pump_electric.icons = {
    {icon="__angelsrefining__/graphics/icons/seafloor-pump-ico.png"},
}
seafloor_pump_electric.icon_size = 32
seafloor_pump_electric.minable = {mining_time = 0.1, result = "seafloor-pump"}
seafloor_pump_electric.placeable_by = {item='seafloor-pump', count=1}
seafloor_pump_electric.flags = {"placeable-neutral", "player-creation", "filter-directions"}
seafloor_pump_electric.collision_mask = { "ground-tile", "object-layer" }
seafloor_pump_electric.adjacent_tile_collision_test = { "water-tile" }
seafloor_pump_electric.max_health = 80
seafloor_pump_electric.corpse = "small-remnants"
seafloor_pump_electric.resistances = {{type = "fire",percent = 70},{type = "impact", percent = 30}}
seafloor_pump_electric.collision_box = seafloor_pump.collision_box
seafloor_pump_electric.selection_box = seafloor_pump.selection_box
seafloor_pump_electric.fast_replaceable_group = seafloor_pump.fast_replaceable_group
seafloor_pump_electric.next_upgrade = nil
seafloor_pump_electric.trigger_created_entity = true
seafloor_pump_electric.crafting_categories = {"apm_fluids_from_the_void"}
seafloor_pump_electric.crafting_speed = 1
seafloor_pump_electric.fixed_recipe = "apm_water_viscous_mud_from_void"
seafloor_pump_electric.show_recipe_icon = false
seafloor_pump_electric.energy_usage = apm.angel_addon.constants.energy_usage.seafloor_pump
seafloor_pump_electric.energy_source = {}
seafloor_pump_electric.energy_source.type = 'electric'
seafloor_pump_electric.energy_source.usage_priority = "secondary-input"
seafloor_pump_electric.energy_source.emissions_per_minute = apm.angel_addon.constants.emissions.seafloor_pump
seafloor_pump_electric.module_specification = apm.angel_addon.constants.modules.specification_seafloor_pump
seafloor_pump_electric.allowed_effects = apm.angel_addon.constants.modules.allowed_effects_seafloor_pump
seafloor_pump_electric.fluid_boxes = {}
seafloor_pump_electric.fluid_boxes[1] = {}
seafloor_pump_electric.fluid_boxes[1].base_area = 1
seafloor_pump_electric.fluid_boxes[1].base_level = 1
seafloor_pump_electric.fluid_boxes[1].pipe_covers = apm.lib.utils.pipecovers.pipecoverspictures()
seafloor_pump_electric.fluid_boxes[1].production_type = "output"
seafloor_pump_electric.fluid_boxes[1].filter = "water-viscous-mud"
seafloor_pump_electric.fluid_boxes[1].pipe_connections = {}
seafloor_pump_electric.fluid_boxes[1].pipe_connections[1] = {}
seafloor_pump_electric.fluid_boxes[1].pipe_connections[1].position = {0, 1}
seafloor_pump_electric.fluid_boxes[1].pipe_connections[1].type = "output"
seafloor_pump_electric.fluid_boxes[1].off_when_no_fluid_recipe = false
seafloor_pump_electric.tile_width = 3
seafloor_pump_electric.tile_height = 1
seafloor_pump_electric.vehicle_impact_sound =  { filename = "__base__/sound/car-metal-impact.ogg", volume = 0.65 }
seafloor_pump_electric.animation = {}
seafloor_pump_electric.animation.north = {}
seafloor_pump_electric.animation.north.layers = {}
seafloor_pump_electric.animation.north.layers[1] = {}
seafloor_pump_electric.animation.north.layers[1].filename = "__angelsrefining__/graphics/entity/seafloor-pump/seafloor-pump.png"
seafloor_pump_electric.animation.north.layers[1].priority = "high"
seafloor_pump_electric.animation.north.layers[1].shift = {0, -1}
seafloor_pump_electric.animation.north.layers[1].width = 160
seafloor_pump_electric.animation.north.layers[1].height = 160
seafloor_pump_electric.animation.north.layers[1].frame_count = 1
seafloor_pump_electric.animation.north.layers[1].line_length = 1
seafloor_pump_electric.animation.north.layers[1].animation_speed = 1
seafloor_pump_electric.animation.east = {}
seafloor_pump_electric.animation.east.layers = {}
seafloor_pump_electric.animation.east.layers[1] = {}
seafloor_pump_electric.animation.east.layers[1].filename = "__angelsrefining__/graphics/entity/seafloor-pump/seafloor-pump.png"
seafloor_pump_electric.animation.east.layers[1].priority = "high"
seafloor_pump_electric.animation.east.layers[1].shift = {1, 0}
seafloor_pump_electric.animation.east.layers[1].x = 160
seafloor_pump_electric.animation.east.layers[1].width = 160
seafloor_pump_electric.animation.east.layers[1].height = 160
seafloor_pump_electric.animation.east.layers[1].frame_count = 1
seafloor_pump_electric.animation.east.layers[1].line_length = 1
seafloor_pump_electric.animation.east.layers[1].animation_speed = 1
seafloor_pump_electric.animation.south = {}
seafloor_pump_electric.animation.south.layers = {}
seafloor_pump_electric.animation.south.layers[1] = {}
seafloor_pump_electric.animation.south.layers[1].filename = "__angelsrefining__/graphics/entity/seafloor-pump/seafloor-pump.png"
seafloor_pump_electric.animation.south.layers[1].priority = "high"
seafloor_pump_electric.animation.south.layers[1].shift = {0, 1}
seafloor_pump_electric.animation.south.layers[1].x = 320
seafloor_pump_electric.animation.south.layers[1].width = 160
seafloor_pump_electric.animation.south.layers[1].height = 160
seafloor_pump_electric.animation.south.layers[1].frame_count = 1
seafloor_pump_electric.animation.south.layers[1].line_length = 1
seafloor_pump_electric.animation.south.layers[1].animation_speed = 1
seafloor_pump_electric.animation.west = {}
seafloor_pump_electric.animation.west.layers = {}
seafloor_pump_electric.animation.west.layers[1] = {}
seafloor_pump_electric.animation.west.layers[1].filename = "__angelsrefining__/graphics/entity/seafloor-pump/seafloor-pump.png"
seafloor_pump_electric.animation.west.layers[1].priority = "high"
seafloor_pump_electric.animation.west.layers[1].shift = {-1, 0}
seafloor_pump_electric.animation.west.layers[1].x = 480
seafloor_pump_electric.animation.west.layers[1].width = 160
seafloor_pump_electric.animation.west.layers[1].height = 160
seafloor_pump_electric.animation.west.layers[1].frame_count = 1
seafloor_pump_electric.animation.west.layers[1].line_length = 1
seafloor_pump_electric.animation.west.layers[1].animation_speed = 1
seafloor_pump_electric.placeable_position_visualization = {}
seafloor_pump_electric.placeable_position_visualization.filename = "__core__/graphics/cursor-boxes-32x32.png"
seafloor_pump_electric.placeable_position_visualization.priority = "extra-high-no-scale"
seafloor_pump_electric.placeable_position_visualization.width = 64
seafloor_pump_electric.placeable_position_visualization.height = 64
seafloor_pump_electric.placeable_position_visualization.scale = 0.5
seafloor_pump_electric.placeable_position_visualization.x = 3*64
seafloor_pump_electric.picture = {}
seafloor_pump_electric.picture.circuit_wire_connection_points = circuit_connector_definitions["offshore-pump"].points
seafloor_pump_electric.picture.circuit_connector_sprites = circuit_connector_definitions["offshore-pump"].sprites
seafloor_pump_electric.picture.circuit_wire_max_distance = default_circuit_wire_max_distance
data:extend({seafloor_pump_electric})