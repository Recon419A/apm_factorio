require ('util')
require('__apm_lib__.lib.log')

local self = 'apm_angel_addon/prototypes/integrations/entities.lua'

APM_LOG_HEADER(self)

local apm_angel_addon_casting_machine_recipes = settings.startup["apm_angel_addon_casting_machine_recipes"].value
local apm_angel_addon_strand_casting_machine_recipes = settings.startup["apm_angel_addon_strand_casting_machine_recipes"].value
local apm_angel_addon_casting_recipes = settings.startup["apm_angel_addon_casting_recipes"].value
local apm_angel_addon_always_show_made_in = settings.startup["apm_angel_addon_always_show_made_in"].value
local apm_angel_addon_steam_unification = settings.startup["apm_angel_addon_steam_unification"].value
local apm_angel_addon_compat_bob = settings.startup["apm_angel_addon_compat_bob"].value
local apm_angel_addon_compat_shiny_angel_gfx = settings.startup["apm_angel_addon_compat_shiny_angel_gfx"].value
local apm_angel_addon_electric_seafloor_pump = settings.startup["apm_angel_addon_electric_seafloor_pump"].value

APM_LOG_SETTINGS(self, 'apm_angel_addon_casting_machine_recipes', apm_angel_addon_casting_machine_recipes)
APM_LOG_SETTINGS(self, 'apm_angel_addon_strand_casting_machine_recipes', apm_angel_addon_strand_casting_machine_recipes)
APM_LOG_SETTINGS(self, 'apm_angel_addon_casting_recipes', apm_angel_addon_casting_recipes)
APM_LOG_SETTINGS(self, 'apm_angel_addon_always_show_made_in', apm_angel_addon_always_show_made_in)
APM_LOG_SETTINGS(self, 'apm_angel_addon_steam_unification', apm_angel_addon_steam_unification)
APM_LOG_SETTINGS(self, 'apm_angel_addon_compat_bob', apm_angel_addon_compat_bob)
APM_LOG_SETTINGS(self, 'apm_angel_addon_compat_shiny_angel_gfx', apm_angel_addon_compat_shiny_angel_gfx)
APM_LOG_SETTINGS(self, 'apm_angel_addon_electric_seafloor_pump', apm_angel_addon_electric_seafloor_pump)

if mods['ShinyAngelGFX'] and apm_angel_addon_compat_shiny_angel_gfx then
    apm.angel_addon.casting_machine.update('casting-machine')
end

apm.angel_addon.casting_machine.update('casting-machine-2')
apm.angel_addon.casting_machine.update('casting-machine-3')
apm.angel_addon.casting_machine.update('casting-machine-4')
