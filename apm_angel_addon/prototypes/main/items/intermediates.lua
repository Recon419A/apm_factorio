require('util')
require('__apm_lib__.lib.log')

local self = 'apm_angel_addon/prototypes/main/items/intermediates.lua'

APM_LOG_HEADER(self)

local apm_angel_addon_casting_recipes = settings.startup["apm_angel_addon_casting_recipes"].value
APM_LOG_SETTINGS(self, 'apm_angel_addon_casting_recipes', apm_angel_addon_casting_recipes)

if not apm_angel_addon_casting_recipes then return end

-- Item -----------------------------------------------------------------------
--
--
-- ----------------------------------------------------------------------------
local item = {}
item.type = 'item'
item.name = 'apm_mold_gears'
item.icons = {
    apm.angel_addon.icons.gear_casting_form
}
item.stack_size = 200
item.group = "angels-casting"
item.subgroup = "apm_tools"
item.order = 'aa_a'
data:extend({item})

-- Item -----------------------------------------------------------------------
--
--
-- ----------------------------------------------------------------------------
local item = {}
item.type = 'item'
item.name = 'apm_mold_balls'
item.icons = {
    apm.angel_addon.icons.ball_casting_form
}
item.stack_size = 200
item.group = "angels-casting"
item.subgroup = "apm_tools"
item.order = 'ab_a'
data:extend({item})