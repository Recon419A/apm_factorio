require('util')
require('__apm_lib__.lib.log')

local self = 'apm_angel_addon/prototypes/main/technologies.lua'

APM_LOG_HEADER(self)

local apm_angel_addon_casting_recipes = settings.startup["apm_angel_addon_casting_recipes"].value
APM_LOG_SETTINGS(self, 'apm_angel_addon_casting_recipes', apm_angel_addon_casting_recipes)

if not apm_angel_addon_casting_recipes then return end

-- Technologie ----------------------------------------------------------------
-- 
--
-- ----------------------------------------------------------------------------
apm.lib.utils.technology.new('apm_power',
    'apm_casting_tools',
    {'angels-metallurgy-2', 'chemical-science-pack'},
    {'apm_mold_gears', 'apm_mold_balls'},
    {{"automation-science-pack", 1}, {"logistic-science-pack", 1}, {"chemical-science-pack", 1}},
    100, 30)
apm.lib.utils.technology.mod.icon('apm_casting_tools', '__apm_resource_pack__/graphics/technologies/apm_molds.png')
