#!/usr/bin/env python3.8
# -*- coding: utf-8 -*-

# Factorio Module
# Copyright (C) 2019 Amator Phasma
#
# This script is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import sys
import json
from os import path

__title__ = 'Factorio'
__author__ = 'AmatorPhasma (Bjoern Trimborn)'
__email__ = 'factorio@bjoern-trimborn.de'
__date__ = '2019.12.08'
__license__ = 'GPL v3'
__status__ = 'Stable'


class Factorio(object):
    __path_factorio = ''

    def __init__(self, factorio_path: str = None) -> None:
        if sys.platform == 'win32' and factorio_path is None:
            self.factorio_path = '{}/appdata/roaming/factorio'.format(path.expanduser('~'))
        elif sys.platform == 'linux' and factorio_path is None:
            self.factorio_path = '{}/.local/factorio/'.format(path.expanduser('~'))
        elif factorio_path is not None:
            self.factorio_path = factorio_path

    @property
    def factorio_path(self) -> str:
        _path = self.__path_factorio.replace('\\\\', '/').replace('\\', '/')
        return _path

    @factorio_path.setter
    def factorio_path(self, value: str):
        if not path.isdir(value):
            raise ValueError('value must be an existing directory')
        value = value.replace('\\', '/')
        self.__path_factorio = value

    @staticmethod
    def mod_version(modname: str) -> str:
        info_path = './{}/info.json'.format(modname)
        if not path.isfile(info_path):
            raise OSError('Can\'t find info.json for mod: {} at {}'.format(modname, info_path))
        with open(info_path, 'r') as f:
            mod_info = json.load(f)
            return mod_info.get('version')
