# APM Library API (WIP)
Version 0.19.0 (online version: [https://gitlab.com/AmatorPhasma/apm_factorio/blob/master/doc/interfaces.md](https://gitlab.com/AmatorPhasma/apm_factorio/blob/master/doc/lib.md))

## __[apm.lib.log](https://gitlab.com/AmatorPhasma/apm_factorio/tree/master/apm_lib/lib/log.lua)__
###### `APM_LOG_ERR(caller :: str, func :: str, msg :: str)`
###### `APM_LOG_WARN(caller :: str, func :: str, msg :: str)`
###### `APM_LOG_INFO(caller :: str, func :: str, msg :: str)`
###### `APM_LOG_HEADER(path :: str)`
###### `APM_LOG_SETTINGS(caller :: str, func :: str, msg :: str)`
## __[apm.lib.utils.debug](https://gitlab.com/AmatorPhasma/apm_factorio/tree/master/apm_lib/lib/utils/debug.lua)__
###### `apm.lib.utils.debug.table(t :: table)`
## __[apm.lib.utils.setting](https://gitlab.com/AmatorPhasma/apm_factorio/tree/master/apm_lib/lib/utils/setting.lua)__
###### `apm.lib.utils.setting.get.starup(settings_name :: str)`
## __[apm.lib.utils.math](https://gitlab.com/AmatorPhasma/apm_factorio/tree/master/apm_lib/lib/utils/math.lua)__
###### `apm.lib.utils.math.round(num :: int, numDecimalPlaces :: int)`
## __[apm.lib.utils.string](https://gitlab.com/AmatorPhasma/apm_factorio/tree/master/apm_lib/lib/utils/string.lua)__
###### `apm.lib.utils.string.convert_to_number(string :: str)`
###### `apm.lib.utils.string.version_to_number(version :: str)`
## __[apm.lib.utils.icon](https://gitlab.com/AmatorPhasma/apm_factorio/tree/master/apm_lib/lib/utils/icon.lua)__
###### `apm.lib.utils.icon.get.from_item(object_name)`
###### `apm.lib.utils.icon.get.from_fluid(object_name)`
###### `apm.lib.utils.icon.get.from_recipe(object_name)`
###### `apm.lib.utils.icon.get.from_tool(object_name)`
###### `apm.lib.utils.icon.mod(t_icon, scale, shift, base_size)`
###### `apm.lib.utils.icons.mod(t_icons, scale, shift)`
###### `apm.lib.utils.icon.merge(icon_tables)`
###### `apm.lib.utils.icon.generate.chemical(tint_1, tint_2, tint_3, tint_4, symbol, scale, shift)`
###### `apm.lib.utils.icon.generate.fluid(tint_1, tint_2, symbol, background_tint, background_alpha)`
###### `apm.lib.utils.icon.layer.insert(base_dn, layer, icon_path, icon_size, scale, shift, tint)`
###### `apm.lib.utils.icon.replace(icon_path_old, icon_path_new, icon_size, scale, shift, tint)`
###### `apm.lib.utils.icon.layer.replace(base_dn, layer, icon_path, icon_size, scale, shift, tint)`
###### `apm.lib.utils.icon.add_tier_lable(name, level)`
###### `apm.lib.utils.icon.set.icons(object, t_icons)`
## __[apm.lib.utils.pipecovers](https://gitlab.com/AmatorPhasma/apm_factorio/tree/master/apm_lib/lib/utils/pipecovers.lua)__
###### `apm.lib.utils.pipecovers.assembler1pipepictures()`
###### `apm.lib.utils.pipecovers.assembler2pipepictures()`
###### `apm.lib.utils.pipecovers.assembler3pipepictures()`
###### `apm.lib.utils.pipecovers.assembler4pipepictures()`
###### `apm.lib.utils.pipecovers.pipecoverspictures()`
## __[apm.lib.utils.category](https://gitlab.com/AmatorPhasma/apm_factorio/tree/master/apm_lib/lib/utils/category.lua)__
###### `apm.lib.utils.category.create.group(name :: str, icon :: str, order :: str)`
###### `apm.lib.utils.category.create.subgroup(group :: str, subgroup :: str, order :: str)`
###### `apm.lib.utils.category.change(group :: str, subgroup :: str, new_group :: str, new_subgroup :: str)`
## __[apm.lib.utils.description](https://gitlab.com/AmatorPhasma/apm_factorio/tree/master/apm_lib/lib/utils/description.lua)__
###### `apm.lib.utils.description.entities.exclude_list.add(entity_name :: str)`
###### `apm.lib.utils.description.entities.exclude_list.remove(entity_name :: str)`
###### `apm.lib.utils.description.entities.add_fuel_types(entity :: LuaEntityPrototype, entry_list :: table)`
###### `apm.lib.utils.description.entities.initial(entity :: LuaEntityPrototype)`
###### `apm.lib.utils.description.entities.setup(entity :: LuaEntityPrototype, entry_list :: table)`
###### `apm.lib.utils.description.item.nuclear_fuel.update(item :: LuaItemPrototype or LuaItemPrototype.name)`
###### `apm.lib.utils.description.item.fuel.update(item :: LuaItemPrototype or LuaItemPrototype.name)`
###### `pm.lib.utils.description.entities.update()`
## __[apm.lib.utils.character](https://gitlab.com/AmatorPhasma/apm_factorio/tree/master/apm_lib/lib/utils/character.lua)__
###### `apm.lib.utils.character.crafting_category.add(name :: str, crafting_category :: str)`
## __[apm.lib.utils.item](https://gitlab.com/AmatorPhasma/apm_factorio/tree/master/apm_lib/lib/utils/item.lua)__
###### `apm.lib.utils.item.dummy() -> LuaItemPrototype`
###### `apm.lib.utils.item.exist(item_name :: LuaItemPrototype.name) -> bool`
###### `(!) apm.lib.utils.item.create_simple(module, item_name, item_group, item_sub_group, item_order)`
###### `apm.lib.utils.item.get_type(item_name :: LuaItemPrototype.name, prefer_item :: str [item or fluid]) -> str or nil`
###### `apm.lib.utils.item.add.radioactive_description(item_name :: LuaItemPrototype.name, level :: int [1,2,3])`
###### `apm.lib.utils.item.set.icons(item_name :: LuaItemPrototype.name, icons :: table{Types/IconData})`
###### `apm.lib.utils.item.is_type(item_name :: LuaItemPrototype.name|LuaFluidPrototype.name, item_type: str [item or fluid]) -> bool`
###### `apm.lib.utils.item.mod.stack_size(item_name :: LuaItemPrototype.name, value :: int, b_overwrite :: bool)`
###### `apm.lib.utils.item.delete_hard(item_name :: LuaItemPrototype.name)`
###### `apm.lib.utils.item.remove(item_name :: LuaItemPrototype.name)`
###### `apm.lib.utils.item.mod.remove_fuel_value(item_name :: LuaItemPrototype.name)`
###### `apm.lib.utils.item.mod.fuel_category(item_name :: LuaItemPrototype.name, fuel_category :: str)`
###### `apm.lib.utils.item.has.burnt_result(item_name :: LuaItemPrototype.name, burnt_result :: LuaItemPrototype.name)`
###### `apm.lib.utils.item.replace.burnt_result(item_name :: LuaItemPrototype.name, old_burnt_result :: LuaItemPrototype.name, new_burnt_result :: LuaItemPrototype.name)`
###### `apm.lib.utils.item.replace.burnt_results(old_burnt_result :: LuaItemPrototype.name, new_burnt_result :: LuaItemPrototype.name)`
###### `apm.lib.utils.item.mod.burnt_result(item_name :: LuaItemPrototype.name, burnt_result :: LuaItemPrototype.name)`
###### `apm.lib.utils.item.overwrite.localised_name(item_name :: LuaItemPrototype.name, localised_name :: str)`
###### `apm.lib.utils.item.overwrite.localised_description(item_name :: LuaItemPrototype.name, localised_description :: str)`
###### `apm.lib.utils.item.overwrite.battery(level, item_name :: LuaItemPrototype.name, fuel_value: str [1kJ...], burnt_result :: LuaItemPrototype.name)`
###### `apm.lib.utils.item.overwrite.group(item_name :: LuaItemPrototype.name, group :: str, subgroup :: str, order :: str)`
## __[apm.lib.utils.fluid](https://gitlab.com/AmatorPhasma/apm_factorio/tree/master/apm_lib/lib/utils/fluid.lua)__
###### `apm.lib.utils.fluid.exist(fluid_name :: LuaFluidPrototype.name)`
###### `apm.lib.utils.fluid.remove(fluid_name :: LuaFluidPrototype.name)`
###### `apm.lib.utils.fluid.delete_hard(fluid_name :: LuaFluidPrototype.name)`
## __[apm.lib.utils.entity](https://gitlab.com/AmatorPhasma/apm_factorio/tree/master/apm_lib/lib/utils/entity.lua)__
###### `apm.lib.utils.entity.has.fuel_category(entity, category)`
###### `apm.lib.utils.entity.get.fuel_categories(entity)`
###### `apm.lib.utils.entity.add.fuel_category(entity, category)`
###### `apm.lib.utils.entity.set.fuel_category(entity, categories)`
###### `apm.lib.utils.entity.set.next_upgrade(entity, next_upgrade)`
###### `apm.lib.utils.entity.has.crafting_category(entity, category)`
###### `apm.lib.utils.entity.add.crafting_category(entity, category)`
###### `apm.lib.utils.entity.has.flag(entity, flag)`
###### `apm.lib.utils.entity.add.flag(entity, flag)`
## __[apm.lib.utils.entities](https://gitlab.com/AmatorPhasma/apm_factorio/tree/master/apm_lib/lib/utils/entities.lua)__
###### `apm.lib.utils.entities.add.fuel_category_with_conditional(entity_type, conditional_category, category)`
###### `apm.lib.utils.entities.set.fuel_categoriy_to_all_with_condition(entity_type, conditional_category, categories)`
###### `apm.lib.utils.entities.set.fuel_categoriy_to_all(entity_type, categories)`
## __[apm.lib.utils.boiler](https://gitlab.com/AmatorPhasma/apm_factorio/tree/master/apm_lib/lib/utils/boiler.lua)__
###### `apm.lib.utils.boiler.exist(boiler_name)`
###### `apm.lib.utils.boiler.get.fuel_categories(boiler_name)`
###### `apm.lib.utils.boiler.update_description(boiler_name)`
###### `apm.lib.utils.boiler.set.next_upgrade(boiler_name, next_upgrade)`
###### `apm.lib.utils.boiler.set.hidden(boiler_name)`
###### `apm.lib.utils.boiler.overhaul(boiler_name, level)`
## __[apm.lib.utils.generator](https://gitlab.com/AmatorPhasma/apm_factorio/tree/master/apm_lib/lib/utils/generator.lua)__
###### `apm.lib.utils.generator.exist(generator_name)`
###### `apm.lib.utils.generator.get.fuel_categories(generator_name)`
###### `apm.lib.utils.generator.update_description(generator_name)`
###### `apm.lib.utils.generator.set.next_upgrade(generator_name, next_upgrade)`
###### `apm.lib.utils.generator.overhaul(generator_name, level)`
###### `apm.lib.utils.generator.set.hidden(generator_name)`
## __[apm.lib.utils.furnace](https://gitlab.com/AmatorPhasma/apm_factorio/tree/master/apm_lib/lib/utils/furnace.lua)__
###### `apm.lib.utils.furnace.exist(furnace_name)`
###### `apm.lib.utils.furnace.mod.category.add(furnace_name, category)`
###### `apm.lib.utils.furnace.get.fuel_categories(furnace_name)`
###### `apm.lib.utils.furnace.update_description(furnace_name)`
###### `apm.lib.utils.furnace.overhaul(furnace_name, level, only_refined)`
###### `apm.lib.utils.furnace.set.hidden(furnace_name)`
## __[apm.lib.utils.inserter](https://gitlab.com/AmatorPhasma/apm_factorio/tree/master/apm_lib/lib/utils/inserter.lua)__
###### `apm.lib.utils.inserter.exist(inserter_name)`
###### `apm.lib.utils.inserter.get.fuel_categories(inserter_name)`
###### `apm.lib.utils.inserter.update_description(inserter_name)`
###### `apm.lib.utils.inserter.burner.overhaul(inserter_name)`
## __[apm.lib.utils.mining_drill](https://gitlab.com/AmatorPhasma/apm_factorio/tree/master/apm_lib/lib/utils/mining_drill.lua)__
###### `apm.lib.utils.mining_drill.exist(mining_drill_name)`
###### `apm.lib.utils.mining_drill.get.fuel_categories(mining_drill_name)`
###### `apm.lib.utils.mining_drill.update_description(mining_drill_name)`
###### `apm.lib.utils.mining_drill.burner.overhaul(mining_drill_name, level)`
###### `apm.lib.utils.mining_drill.set.hidden(mining_drill_name)`
## __[apm.lib.utils.assembler](https://gitlab.com/AmatorPhasma/apm_factorio/tree/master/apm_lib/lib/utils/assembler.lua)__
###### `apm.lib.utils.assembler.exist(assembler_name)`
###### `apm.lib.utils.assembler.get.fuel_categories(assembler_name)`
###### `apm.lib.utils.assembler.update_description(assembler_name)`
###### `apm.lib.utils.assembler.mod.category.add(assembler_name, category)`
###### `apm.lib.utils.assembler.mod.crafting_speed(assembler_name, value)`
###### `apm.lib.utils.assembler.mod.module_specification(assembler_name, value, allowed_effects)`
###### `apm.lib.utils.assembler.add.fluid_connections(assembler_name, level)`
###### `apm.lib.utils.assembler.burner.add_fuel_category(assembler_name, category)`
###### `apm.lib.utils.assembler.burner.overhaul(assembler_name, only_refined)`
###### `apm.lib.utils.assembler.centrifuge.overhaul(centrifuge_name, level)`
###### `apm.lib.utils.assembler.set.hidden(assembler_name)`
## __[apm.lib.utils.reactor](https://gitlab.com/AmatorPhasma/apm_factorio/tree/master/apm_lib/lib/utils/reactor.lua)__
###### `apm.lib.utils.reactor.exist(reactor_name)`
###### `apm.lib.utils.reactor.overhaul_exceptions.add(reactor_name)`
###### `apm.lib.utils.reactor.overhaul_exceptions.remove(reactor_name)`
###### `apm.lib.utils.reactor.get.fuel_categories(reactor_name)`
###### `apm.lib.utils.reactor.update_description(reactor_name)`
###### `apm.lib.utils.reactor.add.fuel_category(reactor_name, fuel_categorie)`
###### `apm.lib.utils.reactor.set.fuel_categories(reactor_name, fuel_categories)`
###### `apm.lib.utils.reactor.overhaul(reactor_name, level)`
## __[apm.lib.utils.lab](https://gitlab.com/AmatorPhasma/apm_factorio/tree/master/apm_lib/lib/utils/lab.lua)__
###### `apm.lib.utils.lab.exist(lab_name)`
###### `apm.lib.utils.lab.get.fuel_categories(lab_name)`
###### `apm.lib.utils.lab.update_description(lab_name)`
###### `apm.lib.utils.lab.overhaul(lab_name)`
###### `apm.lib.utils.lab.add.science_pack(lab_name, science_pack)`
###### `apm.lib.utils.lab.inputs(lab_name, t_science_packs)`
## __[apm.lib.utils.pump](https://gitlab.com/AmatorPhasma/apm_factorio/tree/master/apm_lib/lib/utils/pump.lua)__
###### `apm.lib.utils.pump.exist(pump_name)`
###### `apm.lib.utils.pump.get.fuel_categories(pump_name)`
###### `apm.lib.utils.pump.update_description(pump_name)`
## __[apm.lib.utils.storage_tank](https://gitlab.com/AmatorPhasma/apm_factorio/tree/master/apm_lib/lib/utils/storage_tank.lua)__
###### `apm.lib.utils.storage_tank.exist(storage_tank_name)`
###### `apm.lib.utils.storage_tank.set.hidden(storage_tank_name)`
## __[apm.lib.utils.car](https://gitlab.com/AmatorPhasma/apm_factorio/tree/master/apm_lib/lib/utils/car.lua)__
###### `apm.lib.utils.car.exist(car_name)`
###### `apm.lib.utils.car.get.fuel_categories(car_name)`
###### `apm.lib.utils.car.update_description(car_name)`
###### `apm.lib.utils.car.overhaul(car_name)`
###### `apm.lib.utils.car.effectivity(car_name, value)`
###### `apm.lib.utils.car.overhaul_all()`
###### `apm.lib.utils.car.add.fuel_category(car_name, category)`
###### `apm.lib.utils.car.set.fuel_category(car_name, categories)`
## __[apm.lib.utils.locomotive](https://gitlab.com/AmatorPhasma/apm_factorio/tree/master/apm_lib/lib/utils/locomotive.lua)__
###### `apm.lib.utils.locomotive.exist(locomotive_name)`
###### `apm.lib.utils.locomotive.get.fuel_categories(locomotive_name)`
###### `apm.lib.utils.locomotive.update_description(locomotive_name)`
###### `apm.lib.utils.locomotive.add.fuel_category(locomotive_name, fuel_category)`
###### `apm.lib.utils.locomotive.overhaul(locomotive_name)`
###### `apm.lib.utils.locomotive.overhaul_all()`
## __[apm.lib.utils.turret](https://gitlab.com/AmatorPhasma/apm_factorio/tree/master/apm_lib/lib/utils/turret.lua)__
###### `apm.lib.utils.turret.exist(turret_name)`
###### `(!) apm.lib.utils.turret.overhaul(turret_name, only_refined)`
## __[apm.lib.utils.bot](https://gitlab.com/AmatorPhasma/apm_factorio/tree/master/apm_lib/lib/utils/bot.lua)__
###### `apm.lib.utils.bot.logistic.exist(bot_name)`
###### `apm.lib.utils.bot.construction.exist(bot_name)`
###### `apm.lib.utils.bot.logistic.overhaul(bot_name, level)`
###### `apm.lib.utils.bot.construction.overhaul(bot_name, level)`
## __[apm.lib.utils.fuel](https://gitlab.com/AmatorPhasma/apm_factorio/tree/master/apm_lib/lib/utils/fuel.lua)__
###### `apm.lib.utils.fuel.add.to_exlude_list(entity_name)`
###### `apm.lib.utils.fuel.get_base_fuel_value()`
###### `apm.lib.utils.fuel.overwrite_coal_fuel_value()`
###### `apm.lib.utils.fuel.get_coal_fuel_value()`
###### `apm.lib.utils.fuel.overwrite_emissions_multiplier(item_name, emissions_multiplier)`
###### `apm.lib.utils.fuel.overhaul(level, item_name, multiplicator, burnt_result, fuel_category)`
###### `apm.lib.utils.fuel.category.create(category_name)`
## __[apm.lib.utils.recipe](https://gitlab.com/AmatorPhasma/apm_factorio/tree/master/apm_lib/lib/utils/recipe.lua)__
###### `apm.lib.utils.recipe.exist(recipe_name)`
###### `apm.lib.utils.recipe.allow_productivity(recipe_name)`
###### `apm.lib.utils.recipe.has.result(recipe_name, result_name)`
###### `apm.lib.utils.recipe.has.ingredient(recipe_name, ingredient_name)`
###### `apm.lib.utils.recipe.clone(recipe_name, recipe_name_new)`
###### `apm.lib.utils.recipe.remove(recipe_name)`
###### `apm.lib.utils.recipe.disable(recipe_name)`
###### `apm.lib.utils.recipe.enable(recipe_name)`
###### `apm.lib.utils.recipe.convert_simple_result_to_results(recipe_name)`
###### `apm.lib.utils.recipe.ingredient.remove_all(recipe_name)`
###### `apm.lib.utils.recipe.ingredient.mod(recipe_name, ingredient_name, ingredient_amount, ingredient_amount_expensive)`
###### `apm.lib.utils.recipe.ingredient.mod_temperature(recipe_name, ingredient_name, condition_temperature, target_temperature)`
###### `apm.lib.utils.recipe.ingredient.replace(recipe_name, ingredient_old, ingredient_new, amount_multi)`
###### `apm.lib.utils.recipe.ingredient.replace_all(ingredient_old, ingredient_new)`
###### `apm.lib.utils.recipe.has.main_product(recipe_name)`
###### `apm.lib.utils.recipe.get.main_product(recipe_name)`
###### `apm.lib.utils.recipe.set.always_show_products(recipe_name, bool, category_condition)`
###### `apm.lib.utils.recipe.set.always_show_made_in(recipe_name, bool, category_condition)`
###### `apm.lib.utils.recipe.set.icons(recipe_name, icons)`
###### `apm.lib.utils.recipe.set.icon(recipe_name, icon_path)`
###### `apm.lib.utils.recipe.set.main_product(recipe_name, result_old, result_new, force)`
###### `apm.lib.utils.recipe.set.hidden(recipe_name, bool)`
###### `apm.lib.utils.recipe.result.count(recipe_name)`
###### `apm.lib.utils.recipe.result.get_first_result(recipe_name)`
###### `apm.lib.utils.recipe.result.replace(recipe_name, result_old, result_new, amount_multi)`
###### `apm.lib.utils.recipe.result.replace_all(result_old, result_new)`
###### `apm.lib.utils.recipe.add_mainproduct_if_needed(recipe_name, force_mainproduct)`
###### `apm.lib.utils.recipe.result.add_with_probability(recipe_name, result_name, result_amount_min, result_amount_max, probability)`
###### `apm.lib.utils.recipe.result.mod(recipe_name, result_name, result_amount)`
###### `apm.lib.utils.recipe.result.mod_temperature(recipe_name, result_name, condition_temperature, target_temperature)`
###### `apm.lib.utils.recipe.result.remove_all(result_name)`
###### `apm.lib.utils.recipe.category.create(category_name)`
###### `apm.lib.utils.recipe.category.change(recipe_name, category_name)`
###### `apm.lib.utils.recipe.category.overwrite_all(category_name_old, category_name_new)`
###### `apm.lib.utils.recipe.energy_required.mod(recipe_name, value, value_expensive)`
###### `apm.lib.utils.recipe.overwrite.group(recipe_name, group, subgroup, order)`
## __[apm.lib.utils.resource](https://gitlab.com/AmatorPhasma/apm_factorio/tree/master/apm_lib/lib/utils/resource.lua)__
###### `apm.lib.utils.resource.exist(resource_name)`
## __[apm.lib.utils.modules](https://gitlab.com/AmatorPhasma/apm_factorio/tree/master/apm_lib/lib/utils/modules.lua)__
###### `apm.lib.utils.modules.exist(module_name)`
###### `apm.lib.utils.modules.has_productivity(module_name)`
###### `apm.lib.utils.modules.remove_recipe_from_limitations(recipe_name)`
###### `apm.lib.utils.modules.create.category(category_name)`
## __[apm.lib.utils.technology](https://gitlab.com/AmatorPhasma/apm_factorio/tree/master/apm_lib/lib/utils/technology.lua)__
###### `apm.lib.utils.technology.exist(technology_name)`
###### `apm.lib.utils.technology.has.prerequisites(technology_name, prerequisites_name)`
###### `apm.lib.utils.technology.get.prerequisites(technology_name)`
###### `apm.lib.utils.technology.has.science_pack(technology_name, science_pack_name)`
###### `apm.lib.utils.technology.prerequisite.has.science_pack(technology_name, science_pack_name)`
###### `apm.lib.utils.technology.find.technology_by_recipe(recipe_name)`
###### `apm.lib.utils.technology.new(mod_name, technology, t_prerequisites, t_recipes, t_research_packs, i_research_count, i_research_time)`
###### `apm.lib.utils.technology.mod.unit_time(technology_name, time)`
###### `apm.lib.utils.technology.mod.unit_count(technology_name, count)`
###### `apm.lib.utils.technology.mod.order(technology_name, order)`
###### `apm.lib.utils.technology.mod.icon(technology_name, icon)`
###### `apm.lib.utils.technology.add.science_pack(technology_name, science_pack, science_amount)`
###### `apm.lib.utils.technology.check_if_recipe_is_in_unlock(technology_name, recipe_name)`
###### `apm.lib.utils.technology.add.recipe_for_unlock(technology_name, recipe_name)`
###### `apm.lib.utils.technology.add.prerequisites(technology_name, prerequisites_name)`
###### `apm.lib.utils.technology.remove.recipe_from_unlock(technology_name, recipe_name)`
###### `apm.lib.utils.technology.remove.science_pack(technology_name, science_pack)`
###### `apm.lib.utils.technology.remove.prerequisites(technology_name, prerequisites_name)`
###### `apm.lib.utils.technology.disable(technology_name)`
###### `apm.lib.utils.technology.delete(technology_name)`
###### `apm.lib.utils.technology.remove.recipe_recursive(recipe_name, tech_name)`
###### `apm.lib.utils.technology.add.science_pak_conditional(science_pack_name, cond_science_pack_name)`
###### `apm.lib.utils.technology.set.heritage_science_packs_from_prerequisites(technology_name)`
###### `apm.lib.utils.technology.overwrite.localised_name(technology_name, localised_name)`
###### `apm.lib.utils.technology.overwrite.localised_description(technology_name, localised_description)`
###### `apm.lib.utils.technology.force.recipe_for_unlock(technology_name, recipe_name)`
###### `apm.lib.utils.technology.force.prerequisites(technology_name, prerequisites_names)`
## __[apm.lib.utils.batteries](https://gitlab.com/AmatorPhasma/apm_factorio/tree/master/apm_lib/lib/utils/batteries.lua)__
###### `apm.lib.utils.batteries.generate(level, battery_name, fuel_value, overlay, probability, technology_name)`
## __[apm.lib.utils.starfall](https://gitlab.com/AmatorPhasma/apm_factorio/tree/master/apm_lib/lib/utils/starfall.lua)__
###### `apm.lib.utils.starfall.add.ore(ore_name, t_tint, probability, required_fluid)`
###### `apm.lib.utils.starfall.remove.ore(ore_name)`
###### `apm.lib.utils.starfall.ore.generate()`
## __[apm.lib.utils.tile](https://gitlab.com/AmatorPhasma/apm_factorio/tree/master/apm_lib/lib/utils/tile.lua)__
###### `apm.lib.utils.tile.exist(tile_name)`
###### `apm.lib.utils.tile.get.layer(tile_name)`
###### `apm.lib.utils.tile.set.layer(tile_name, layer)`
###### `apm.lib.utils.tile.unification(old_tile_name, new_tile_name)`
###### `apm.lib.utils.tile.set.relation(tile_name, base_tile_name, relation)`
## __[apm.lib.utils.builder](https://gitlab.com/AmatorPhasma/apm_factorio/tree/master/apm_lib/lib/utils/builder.lua)__
###### `apm.lib.utils.builder.recipe.item.alternatives.add(name, alternative, weighting, item_multi)`
###### `apm.lib.utils.builder.recipe.item.alternatives.get(name, weight_min, weight_max)`
###### `apm.lib.utils.builder.recipe.item.name(name, weight_min, weight_max)`
###### `apm.lib.utils.builder.recipe.item.type(name, weight_min, weight_max)`
###### `apm.lib.utils.builder.recipe.item.simple(name, amount, weight_min, weight_max, fluid_multi)`
###### `apm.lib.utils.builder.recipe.item.probability(name, amount_min, amount_max, probability, weight_min, weight_max, fluid_multi)`
###### `apm.lib.utils.builder.recipe.update()`
## __[apm.lib.utils.recycling](https://gitlab.com/AmatorPhasma/apm_factorio/tree/master/apm_lib/lib/utils/recycling.lua)__
###### `apm.lib.utils.recycling.metal.exist(name)`
###### `apm.lib.utils.recycling.metal.add(name, tint, output, output_category, wight, output_probability, t_catalysts, t_output_byproducts, b_own_tech, t_tech_prerequisites, output_amount_overwrite)`
###### `apm.lib.utils.recycling.metal.remove(name)`
###### `apm.lib.utils.recycling.metal.generation()`
###### `apm.lib.utils.recycling.scrap.add(recipe_name, scrap_metal, probability)`
###### `apm.lib.utils.recycling.scrap.remove(recipe_name)`
###### `apm.lib.utils.recycling.scrap.generate()`